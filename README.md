## Focal Fossa Config

Automate installation of programs and my favorite ubuntu settings


##### RUN:

```
chmod +x run.sh
sudo bash run.sh
```

##### REFERENCES:
- [cargospotted wallpaper](https://www.youtube.com/watch?v=dt8KuKe1bfk)

- [jimanjel](https://github.com/jimangel/ubuntu-18.04-scripts)
