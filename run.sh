username="vinicius"
git_email=""
git_username=""


# versions
goVersion="1.14.2"
javaVersion="11"
rstudioVersion='rstudio-1.2.5042-amd64.deb'


# set font colors
red='\033[0;31m'
green='\u001b[32m'
nocolor='\033[0m'


install_essential() {
  sudo apt -y install curl
  sudo apt -y install wget
  sudo apt -y install git
  sudo apt -y install htop
  sudo apt -y install terminator
}


install_neofetch() {
  sudo add-apt-repository ppa:dawidd0811/neofetch
  sudo apt -y update
  sudo apt -y install neofetch
}


install_golang() {
  wget https://dl.google.com/go/go${goVersion}.linux-amd64.tar.gz
  sudo tar -C /usr/local -xzf go${goVersion}.linux-amd64.tar.gz
  rm -rf go${goVersion}.linux-amd64.tar.gz
  cat << 'EOT' >> ~/.bashrc
# set PATH so it includes go bin if it exists
if [ -d "/usr/local/go/bin" ] ; then
  export PATH=$PATH:/usr/local/go/bin
fi
EOT
  # create working dirs
  mkdir -p ~/go/{bin,src,pkg} 2>/dev/null
  printf "${red}!!! MUST RESTART TERMINAL BEFORE GO CAN BE USED${nc}\n"
}


install_java() {
  sudo apt -y install openjdk-${javaVersion}-jdk
}


install_vscode() {
  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
  sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
  sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
  sudo apt install apt-transport-https
  sudo apt update
  sudo apt install code 
}


install_r() {
  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
  sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu eoan-cran35/'
  sudo apt update
  sudo apt -y install r-base
  
  
  sudo apt install -y libxml2-dev
  sudo apt install -y libcurl4-openssl-dev
}


install_rstudio() {
  wget https://download1.rstudio.org/desktop/bionic/amd64/${rstudioVersion}
  sudo apt -y install ./${rstudioVersion}
  rm ./${rstudioVersion}
}


install_google_chrome() {
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo dpkg -i google-chrome-stable_current_amd64.deb
  rm google-chrome-stable_current_amd64.deb
}


install_kubectl() {
  # Download the latest release
  curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
  # Make the kubectl binary executable
  chmod +x ./kubectl
  # Move the binary in to your PATH.
  sudo mv ./kubectl /usr/local/bin/kubectl
  # Test
  kubectl version --client
}


install_virtualbox() {
  sudo apt -y install virtualbox
  echo ""
  printf "${green}REMINDER: Turn VT-x on in the BIOS for all CPU modes before running virtualbox or minikube! ${nc}\n"
}


install_minikube() {
  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
  chmod +x minikube
  sudo install minikube /usr/local/bin/
}

install_docker() {
  sudo apt -y install docker.io
  sudo systemctl enable --now docker
  docker --version
  docker run hello-world
}


configure_system_settings() {
  # configure git environment
  mkdir ~/git 2>/dev/null
  git config --global user.email "${git_email}"
  git config --global user.name "${git_username}"
  
  # setting the wallpaper
  currdir=`pwd`
  gsettings set org.gnome.desktop.background picture-uri ${currdir}/wallpaper.png
  
  # setting my favorite apps
  dconf write /org/gnome/shell/favorite-apps "['terminator.desktop', 'org.gnome.Terminal.desktop', 'firefox.desktop', 'google-chrome.desktop', 'virtualbox.desktop', 'rstudio.desktop', 'code.desktop', 'org.gnome.Nautilus.desktop']"
  
  # put dock at bottom
  gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
}


configure_terminator_dracula_theme() {
    if [ ! -d ~/.config/terminator ]; then 	
       mkdir ~/.config/terminator
    else
       if [ -f ~/.config/terminator/config ]; then
         rm ~/.config/terminator/config
       fi
    fi
    cat ./terminator-dracula-theme >> ~/.config/terminator/config
}



# Começando a baixaria

if [ `id -u` -ne 0 ]; then
	echo Need sudo
	exit 1
fi

printf "${green}Updating and upgrading ... ${nc}\n"
apt update -y
apt upgrade -y

printf "${green}Instaling essential programs ... ${nc}\n"
install_essential

printf "${green}Instaling neofetch ... ${nc}\n"
install_neofetch

printf "${green}Instaling golang ... ${nc}\n"
install_golang

printf "${green}Instaling java ... ${nc}\n"
install_java

printf "${green}Instaling vscode ... ${nc}\n"
install_vscode

printf "${green}Instaling r ... ${nc}\n"
install_r

printf "${green}Instaling rstudio ... ${nc}\n"
install_rstudio

printf "${green}Instaling google chrome ... ${nc}\n"
install_google_chrome

printf "${green}Instaling essential minikube environment ... ${nc}\n"
install_kubectl
install_virtualbox
install_minikube

printf "${green}Instaling Docker ... ${nc}\n"
install_docker

configure_system_settings
configure_terminator_dracula_theme
neofetch
